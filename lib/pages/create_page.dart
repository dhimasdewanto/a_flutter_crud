import 'package:a_flutter_crud/components/keyboard_hider.dart';
import 'package:a_flutter_crud/pages/index_page.dart';
import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:a_flutter_crud/services/buku_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'create_edit_page/create_edit_form.dart';

class CreatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Buku"),
      ),
      body: SafeArea(
        child: KeyboardHider(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              child: _CreateForm()),
        ),
      ),
    );
  }
}

class _CreateForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreateFormState();
}

class _CreateFormState extends State<_CreateForm> {
  final _bukuService = BukuService();
  final _formKey = GlobalKey<FormState>();
  var _judulController = TextEditingController();
  var _kategoriController = TextEditingController();
  var _hargaController = TextEditingController();
  var _isLoading = false;
  var _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return CreateEditForm(
      formKey: _formKey,
      judulController: _judulController,
      kategoriController: _kategoriController,
      hargaController: _hargaController,
      isLoading: _isLoading,
      autoValidate: _autoValidate,
      submitAction: _tambahBuku,
      title: "Tambah Buku",
      isEditPage: false,
    );
  }

  Future<void> _tambahBuku() async {
    if (_formKey.currentState.validate() == false) {
      setState(() {
        _autoValidate = true;
      });
      return;
    }

    setState(() {
      _isLoading = true;
    });
    
    final buku = BukuModel(
      judul: _judulController.text,
      kategori: _kategoriController.text,
      harga: double.parse(_hargaController.text),
    );

    _bukuService.postNewBuku(buku).then((bool isSuccess) {
      _isLoading = false;

      if (isSuccess == false) {
        final snackbar = SnackBar(content: Text('Error Menambahkan Buku'));
        Scaffold.of(context).showSnackBar(snackbar);
        return;
      }

      final route =
          MaterialPageRoute(builder: (BuildContext context) => IndexPage());
      Navigator.push(context, route);
    });
  }
}
