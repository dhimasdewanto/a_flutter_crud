import 'package:a_flutter_crud/components/shimmer_shape.dart';
import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:flutter/material.dart';

class ListBukuCell extends StatelessWidget {
  final BukuModel buku;

  ListBukuCell(this.buku);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            _getWidgetGambar(),
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _getWidgetJudul(),
                  _getWidgetKategori(),
                ],
              ),
            ),
          ],
        ),
        Divider(
          height: 0,
        )
      ],
    );
  }

  Widget _getWidgetJudul() {
    if (buku == null) {
      return ShimmerShape(
        height: 18.0,
        width: 200.0,
      );
    }

    return Text(buku.judul,
      style: TextStyle(
        fontSize: 18.0
      ),
    );
  }

  Widget _getWidgetKategori() {
    if (buku == null) {
      return ShimmerShape();
    }

    return Text(buku.kategori);
  }

  Widget _getWidgetGambar() {
    if (buku == null) {
      return _loadingImagePlaceholder();
    }
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        _loadingImagePlaceholder(),
        Hero(
          tag: buku.id.toString(),
          child: Image.network(
            buku.gambarBuku,
            height: 80.0,
            width: 140.0,
          )
        ),
      ],
    );
  }

  ShimmerShape _loadingImagePlaceholder() {
    return ShimmerShape(
      height: 80.0,
      width: 140.0,
      borderRadius: 0.0,
      verticalPadding: 0.0,
    );
  }
}
