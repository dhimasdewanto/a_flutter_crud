import 'package:a_flutter_crud/components/keyboard_hider.dart';
import 'package:a_flutter_crud/pages/index_page.dart';
import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:a_flutter_crud/services/buku_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'create_edit_page/create_edit_form.dart';

class EditPage extends StatelessWidget {
  final BukuModel editedBuku;

  EditPage(this.editedBuku);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ubah ${editedBuku.judul}"),
      ),
      body: SafeArea(
        child: KeyboardHider(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              child: _EditForm(editedBuku)),
        ),
      ),
    );
  }
}

class _EditForm extends StatefulWidget {
  final BukuModel editedBuku;

  _EditForm(this.editedBuku);

  @override
  State<StatefulWidget> createState() => _EditFormState(editedBuku);
}

class _EditFormState extends State<_EditForm> {
  final BukuModel editedBuku;
  final _bukuService = BukuService();
  final _formKey = GlobalKey<FormState>();
  var _judulController = TextEditingController();
  var _kategoriController = TextEditingController();
  var _hargaController = TextEditingController();
  var _isLoading = false;
  var _autoValidate = false;

  _EditFormState(this.editedBuku);

  @override
  void initState() {
    super.initState();
    _judulController.text = editedBuku.judul;
    _kategoriController.text = editedBuku.kategori;
    _hargaController.text = editedBuku.harga.toString();
  }

  @override
  Widget build(BuildContext context) {
    return CreateEditForm(
      formKey: _formKey,
      judulController: _judulController,
      kategoriController: _kategoriController,
      hargaController: _hargaController,
      isLoading: _isLoading,
      autoValidate: _autoValidate,
      submitAction: _updateBuku,
      title: "Edit ${editedBuku.judul}",
      isEditPage: true,
    );
  }

  Future<void> _updateBuku() async {
    if (_formKey.currentState.validate() == false) {
      setState(() {
        _autoValidate = true;
      });
      return;
    }

    setState(() {
      _isLoading = true;
    });

    final buku = BukuModel(
      id: editedBuku.id,
      judul: _judulController.value.text,
      kategori: _kategoriController.value.text,
      harga: double.parse(_hargaController.value.text),
    );

    _bukuService.deleteBuku(buku.id).then((bool isSuccess) {
      if (isSuccess == false) {
        final snackbar = SnackBar(content: Text('Error Mengupdate Buku'));
        Scaffold.of(context).showSnackBar(snackbar);
        return;
      }

      _bukuService.postNewBuku(buku).then((bool isSuccess) {
        _isLoading = false;

        final route =
            MaterialPageRoute(builder: (BuildContext context) => IndexPage());
        Navigator.push(context, route);
      });
    });
  }
}
