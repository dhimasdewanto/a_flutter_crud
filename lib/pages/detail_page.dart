import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:a_flutter_crud/pages/edit_page.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final BukuModel buku;

  DetailPage(this.buku);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(buku.judul)),
      body: _DetailBody(buku),
      floatingActionButton: FloatingActionButton(
        heroTag: "edit_buku",
        child: Icon(Icons.edit),
        onPressed: () {
          final route = MaterialPageRoute(
              builder: (BuildContext context) => EditPage(buku));
          Navigator.push(context, route);
        },
      ),
    );
  }
}

class _DetailBody extends StatelessWidget {
  final BukuModel buku;

  _DetailBody(this.buku);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Hero(
          tag: buku.id.toString(),
          child: Image.network(
            buku.gambarBuku,
            width: double.infinity,
          ),
        ),
        SizedBox(height: 20.0),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                buku.kategori,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10.0),
              Text(
                buku.judul,
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(height: 10.0),
              Text(
                "Rp. ${buku.harga},00",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
