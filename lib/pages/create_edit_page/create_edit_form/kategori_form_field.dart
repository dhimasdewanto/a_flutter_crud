import 'package:flutter/material.dart';

class KategoriFormField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode nodeTo;
  final FocusNode nodeFrom;

  KategoriFormField({
    @required this.controller,
    @required this.nodeTo,
    @required this.nodeFrom,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      textCapitalization: TextCapitalization.words,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(labelText: "Kategori"),
      focusNode: nodeFrom,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(nodeTo);
      },
      validator: (String value) {
        if (value.isEmpty) {
          return "Silahkan masukkan form";
        }
        if (value.length < 3) {
          return "Tidak boleh kurang dari 3";
        }
      },
    );
  }
}