import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final bool isLoading;
  final bool isEditPage;

  LoadingIndicator({
    @required this.isLoading,
    @required this.isEditPage,
  });

  @override
  Widget build(BuildContext context) {
    if(isLoading == false) {
      return SizedBox();
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 20.0,
          width: 20.0,
          child: CircularProgressIndicator(
            // CircularProgressIndicator color (not using backgroundColor)
            valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
          ),
        ),
        SizedBox(width: 15.0),
        Text(_getIndicatorText())
      ],
    );
  }

  String _getIndicatorText() {
    if (isEditPage) {
      return "Sedang Mengupdate Buku...";
    }
    return "Sedang Menambahkan Buku...";
  }
}