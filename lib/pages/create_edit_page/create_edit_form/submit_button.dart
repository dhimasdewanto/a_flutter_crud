import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  final bool isEditPage;
  final Function onPressedAction;

  SubmitButton({
    @required this.isEditPage,
    @required this.onPressedAction,
  });

  @override
  Widget build(BuildContext context) {
    if (isEditPage) {
      return FloatingActionButton.extended(
        label: Text("Ubah"),
        icon: Icon(Icons.edit),
        heroTag: "edit_buku",
        onPressed: onPressedAction,
      );
    }

    return FloatingActionButton.extended(
      label: Text("Tambah"),
      icon: Icon(Icons.add),
      heroTag: "tambah_buku",
      onPressed: onPressedAction,
    );
  }
}