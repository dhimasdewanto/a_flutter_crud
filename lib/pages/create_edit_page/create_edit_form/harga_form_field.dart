import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HargaFormField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode nodeFrom;
  final Function submittedAction;

  HargaFormField({
    @required this.controller,
    @required this.nodeFrom,
    @required this.submittedAction,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(labelText: "Harga"),
      focusNode: nodeFrom,
      onFieldSubmitted: (value) {
        // Remove Focus on Keyboard
        SystemChannels.textInput.invokeMethod('TextInput.hide');

        submittedAction();
      },
      validator: (String value) {
        if (value.isEmpty) {
          return "Silahkan masukkan form";
        }
        if (double.parse(value) < 20000.0) {
          return "Tidak boleh dibawah 20.000";
        }
      },
    );
  }
}