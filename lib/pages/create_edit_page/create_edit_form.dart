import 'package:flutter/material.dart';

import 'create_edit_form/harga_form_field.dart';
import 'create_edit_form/judul_form_field.dart';
import 'create_edit_form/kategori_form_field.dart';
import 'create_edit_form/loading_indicator.dart';
import 'create_edit_form/submit_button.dart';

class CreateEditForm extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final TextEditingController judulController;
  final TextEditingController kategoriController;
  final TextEditingController hargaController;
  final bool isLoading;
  final bool autoValidate;
  final Function submitAction;
  final String title;
  final bool isEditPage;
  final _nodeToKategori = FocusNode();
  final _nodeToHarga = FocusNode();

  CreateEditForm({
    @required this.formKey,
    @required this.judulController,
    @required this.kategoriController,
    @required this.hargaController,
    @required this.isLoading,
    @required this.autoValidate,
    @required this.submitAction,
    @required this.title,
    @required this.isEditPage,
  });

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidate: autoValidate,
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              title,
              style: Theme.of(context).textTheme.title.copyWith(fontSize: 24.0),
            ),
          ),
          JudulFormField(
            controller: judulController,
            nodeTo: _nodeToKategori,
          ),
          KategoriFormField(
            controller: kategoriController,
            nodeFrom: _nodeToKategori,
            nodeTo: _nodeToHarga,
          ),
          HargaFormField(
            controller: hargaController,
            nodeFrom: _nodeToHarga,
            submittedAction: submitAction,
          ),
          SizedBox(height: 20.0),
          SubmitButton(
            isEditPage: isEditPage,
            onPressedAction: submitAction,
          ),
          SizedBox(height: 20.0),
          LoadingIndicator(
            isEditPage: isEditPage,
            isLoading: isLoading,
          ),
        ],
      ),
    );
  }
}