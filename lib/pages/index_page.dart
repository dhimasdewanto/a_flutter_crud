import 'package:a_flutter_crud/pages/create_page.dart';
import 'package:a_flutter_crud/pages/detail_page.dart';
import 'package:a_flutter_crud/pages/index_page/list_buku_cell.dart';
import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:a_flutter_crud/services/buku_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class IndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Semua Buku"),
        automaticallyImplyLeading: false,
      ),
      body: WillPopScope(
        // Back button to exit
        onWillPop: () async {
          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        },
        child: _IndexPageBody(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        heroTag: "tambah_buku",
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => CreatePage()));
        },
      ),
    );
  }
}

class _IndexPageBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _IndexPageBodyState();
}

class _IndexPageBodyState extends State<_IndexPageBody> {
  final bukuService = BukuService();
  final _listKey = GlobalKey<AnimatedListState>();
  var _listBuku = List<BukuModel>();

  @override
  void initState() {
    super.initState();
    _setListBuku();
  }

  @override
  Widget build(BuildContext context) {
    if (_listBuku.length < 1) {
      return ListView.builder(
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return ListBukuCell(null);
        },
      );
    }

    return RefreshIndicator(
      color: Colors.grey,
      onRefresh: _setListBuku,
      child: AnimatedList(
          key: _listKey,
          initialItemCount: _listBuku.length,
          itemBuilder:
              (BuildContext context, int index, Animation<double> animation) {
            final buku = _listBuku[index];
            return _buildListItem(buku, index, animation);
          }),
    );
  }

  Widget _buildListItem(
      BukuModel buku, int index, Animation<double> animation) {
    return SizeTransition(
      axis: Axis.vertical,
      sizeFactor: animation,
      child: InkWell(
        onTap: () {
          final builder = (BuildContext context) => DetailPage(buku);
          Navigator.push(context, MaterialPageRoute(builder: builder));
        },
        child: Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: ListBukuCell(buku),
          actions: <Widget>[
            IconSlideAction(
              caption: 'Delete',
              foregroundColor: Colors.red,
              icon: Icons.delete,
              onTap: () {
                _deleteBuku(buku, index);
              },
            ),
          ],
        ),
      ),
    );
  }

  void _deleteBuku(BukuModel buku, int index) {
    bukuService.deleteBuku(buku.id).then((isSuccess) {
      if (isSuccess == false) return;

      setState(() {
        final builder = (context, animation) => _buildListItem(buku, index, animation);
        final duration = Duration(milliseconds: 250);
        _listKey.currentState.removeItem(index, builder, duration: duration);
        _listBuku.removeAt(index);
      });

      final snackBar = SnackBar(
        content: Text("Berhasil menghapus ${buku.judul}"),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  Future<void> _setListBuku() async {
    final listBuku = await bukuService.getAllBuku();
    setState(() {
      _listBuku = listBuku;
    });
  }
}
