import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerShape extends StatelessWidget {
  final double height;
  final double width;
  final double borderRadius;
  final double verticalPadding;
  final Color baseColor;
  final Color highlightColor;

  ShimmerShape({
    this.height = 14.0, 
    this.width = 100.0,
    this.borderRadius = 0.0,
    this.verticalPadding = 1.0,
    this.baseColor,
    this.highlightColor,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: verticalPadding),
      child: Shimmer.fromColors(
        baseColor: _getBaseColor(),
        highlightColor: _getHighlightColor(),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
            color: Colors.white,
          ),
          height: height,
          width: width,
        ),
      )
    );
  }

  Color _getBaseColor() {
    if(this.baseColor != null) {
      return this.baseColor;
    }
    return Colors.grey[200];
  }

  Color _getHighlightColor() {
    if(this.highlightColor != null) {
      return this.highlightColor;
    }
    return Colors.white;
  }
}