import 'package:flutter/material.dart';

class NoBackAction extends WillPopScope {
  final Widget child;

  NoBackAction({this.child}) : super(
    onWillPop: () async => false,
    child: child
  );
}