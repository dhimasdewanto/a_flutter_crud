import 'package:flutter/material.dart';

class KeyboardHider extends StatelessWidget {
  final Widget child;

  KeyboardHider({
    @required this.child
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).detach();
      },
      child: child,
    );
  }
}