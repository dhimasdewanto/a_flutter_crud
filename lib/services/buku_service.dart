import 'dart:convert';

import 'package:a_flutter_crud/models/buku_model.dart';
import 'package:http/http.dart' as http;

class BukuService {
  final url = "http://dev.accelist.com:1203/api/exam";
  final headers = {
    "Content-type": "application/json",
  };

  Future<List<BukuModel>> getAllBuku() async {
    final response = await http.get("$url/get-book");

    if (_isSuccess(response.statusCode)) {
      return _jsonToListBuku(response.body);
    }
    
    return List<BukuModel>();
  }

  Future<bool> postNewBuku(BukuModel bukuBaru) async {
    // Remove null id in json
    bukuBaru.id = 0;

    final body = jsonEncode(bukuBaru.toJson());

    final response = await http.post("$url/post-book",
      headers: headers,
      body: body
    );

    return _isSuccess(response.statusCode);
  }

  Future<bool> deleteBuku(int bukuId) async {
    final response = await http.delete("$url/delete-book/$bukuId");

    if (_isSuccess(response.statusCode)) {
      return true;
    }

    return false;
  }

  bool _isSuccess(statusCode) {
    if (statusCode < 200 || statusCode > 299) {
      return false;
    }
    return true;
  }

  List<BukuModel> _jsonToListBuku(String jsonString) {
    final decode = json.decode(jsonString);
    final listMaps = decode.map((item) => BukuModel.fromJson(item));
    return List<BukuModel>.from(listMaps);
  }
}