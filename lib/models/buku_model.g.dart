// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buku_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BukuModel _$BukuModelFromJson(Map<String, dynamic> json) {
  return BukuModel(
      id: json['id'] as int,
      judul: json['judul'] as String,
      kategori: json['kategori'] as String,
      harga: (json['harga'] as num)?.toDouble());
}

Map<String, dynamic> _$BukuModelToJson(BukuModel instance) => <String, dynamic>{
      'id': instance.id,
      'judul': instance.judul,
      'kategori': instance.kategori,
      'harga': instance.harga
    };
