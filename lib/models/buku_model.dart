import 'package:json_annotation/json_annotation.dart';

part 'buku_model.g.dart';

@JsonSerializable()
class BukuModel {
  int id;
  String judul;
  String kategori;
  double harga;
  final gambarBuku = "https://media.npr.org/assets/img/2018/11/18/gettyimages-865109088-170667a_wide-f4e3c4a58ad5e1268dec3654c0b2d490e320bba6-s800-c85.jpg";

  BukuModel({this.id, this.judul, this.kategori, this.harga});

  factory BukuModel.fromJson(Map<String, dynamic> json) => _$BukuModelFromJson(json);

  Map<String, dynamic> toJson() => _$BukuModelToJson(this);
}